#include "polynomial.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void){

	struct term *new1 = make_term(1, 4);
	struct term *new2 = make_term(1, 2);

	struct term *new3 = make_term(1, 3);
	struct term *new4 = make_term(1, 1);	

	struct term *new5 = make_term(3, 5);
	struct term *new6 = make_term(2, 3);
	struct term *new7 = make_term(7, 1);
/*
	struct term *final;
	struct term *final2;
	struct term *final3;
*/
	new1->next = new2;
	new3->next = new4;
	new5->next = new6;
	new6->next = new7;

	//final = add_poly(new1, new3);
	//final2 = sub_poly(final, new3);	
	//final3 = apply_to_each(new5, );
	printf("%s\n", poly_to_string(new1));

	printf("The result is %.02f\n", eval_poly(new1, 2));

/*
	poly_print(final);
	printf("\n");
	poly_print(final2);
	printf("\n");
	poly_print(final3);
	printf("\n");
*/
}
