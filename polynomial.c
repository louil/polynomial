#include "polynomial.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//Provided code
struct term *make_term(int coeff, int exp){

	struct term *node = malloc(sizeof(*node));
	if(node){
		node->coeff = coeff;
		node->exp = exp;
		node->next = NULL;
	}
	return node;
}

//Provided code
void poly_free(polynomial *egn){
	
	while (egn){
		struct term *tmp = egn->next;
		free(egn);
		egn = tmp;
	}
}

//Provided code
void poly_print(polynomial *egn){

	if (!egn){
		return;
	}
	if (egn->coeff){
		printf("%c%d", egn->coeff > 0 ? '+' : '\0', egn->coeff);
		if(egn->exp > 1){
			printf("x^%d", egn->exp);
		}
		else if (egn->exp == 1){
			printf("x");
		}
		printf(" ");
	}
	poly_print(egn->next);
}

//Function that takes a polynomial and converts it to a string to be returned.
char *poly_to_string(polynomial *p){
	
	char *poly = (char *)calloc(sizeof(char)*32, 1);
	char *hold_string = (char *)calloc(sizeof(char)*32, 1);

	//Looping while there is a p, and inside firguring out if the coefficient is positive, negative, or zero and then proceding on to the exponent, eventually getting the formatting correct to be 
	//placed into the hold_string char* that will then eventually be placed inside the poly char*, which is what is returned.
	while(p) {
		if (p->coeff < -1 || p->coeff > 1){
			snprintf(hold_string, 32, "%dx^%d", p->coeff, p->exp);
		}
		else if (p->coeff == 1 || p->coeff == 0){
			if(p->exp > 1 && p->next != NULL){
				snprintf(hold_string, 32, "+x^%d", p->exp);
			}
			else if (p->exp == 1 && p->next != NULL){
				snprintf(hold_string, 32, "-x");
			}
			if(p->exp > 1 && p->next == NULL){
				snprintf(hold_string, 32, "+x^%d", p->exp);
			}
			else if (p->exp == 1 && p->next == NULL){
				snprintf(hold_string, 32, "-x");
			}
		}
		else if (p->coeff == -1){
			if(p->exp > 1 && p->next != NULL){
				snprintf(hold_string, 32, "-x^%d+", p->exp);
			}
			else if (p->exp == 1 && p->next != NULL){
				snprintf(hold_string, 32, "-x");
			}
			if(p->exp > 1 && p->next == NULL){
				snprintf(hold_string, 32, "-x^%d", p->exp);
			}
			else if (p->exp == 1 && p->next == NULL){
				snprintf(hold_string, 32, "-x");
			}
		}

		size_t var = strlen(hold_string)+strlen(poly)+1;
		poly = realloc(poly, var);
		strncat(poly, hold_string, var);
		p = p->next;
	}
	free(hold_string);
	return poly;
}

//Adds to polynomials and returns
polynomial *add_poly(polynomial *a, polynomial *b){

	struct term *node = make_term(0, 0);
	struct term *head = node;

	//While there is a polynomial a or b continue to loop, inside there is checking against the exponents, if they are the same then the coefficients can be added together,
	//at the same time making sure to properly step through setting the node properly while also stepping through the polynomials
	while (a || b){
		if (a && b){
			if (a->exp > b->exp){
				node->coeff = a->coeff;
				node->exp = a->exp;	
				node->next = make_term(0, 0);
				node = node->next;
				a = a->next;
			}
			else if (a->exp < b->exp){
				node->coeff = b->coeff;
				node->exp = b->exp;	
				node->next = make_term(0, 0);
				node = node->next;
				b = b->next;
			}
			else if (a->exp == b->exp){
				node->coeff = a->coeff+b->coeff;
				node->exp = b->exp;
				node->next = make_term(0, 0);
				node = node->next;
				a = a->next;
				b = b->next;
			}
		}
		else if (a){
			node->coeff = a->coeff;
			node->exp = a->exp;
			node->next = make_term(0, 0);
			node = node->next;
			a = a->next;
		}
		else if (b){
			node->coeff = b->coeff;
			node->exp = b->exp;
			node->next = make_term(0, 0);
			node = node->next;
			b = b->next;
		}
	}	
	return head; 
}

//Subtracts a polynomial from another one
polynomial *sub_poly(polynomial *a, polynomial *b){

	struct term *node = make_term(0, 0);
	struct term *head = node;

	//While there is a polynomial a or b continue to loop, inside there is checking against the exponents, if they are the same then the coefficients can be subtracted from one another,
	//at the same time making sure to properly step through setting the node properly while also stepping through the polynomials
	while (a || b){
		if (a && b){
			if (a->exp > b->exp){
				node->coeff = a->coeff;
				node->exp = a->exp;	
				node->next = make_term(0, 0);
				node = node->next;
				a = a->next;
			}
			else if (a->exp < b->exp){
				node->coeff = b->coeff;
				node->exp = b->exp;	
				node->next = make_term(0, 0);
				node = node->next;
				b = b->next;
			}
			else if (a->exp == b->exp){
				node->coeff = a->coeff-b->coeff;
				node->exp = b->exp;
				node->next = make_term(0, 0);
				node = node->next;
				a = a->next;
				b = b->next;
			}
		}
		else if (a){
			node->coeff = a->coeff;
			node->exp = a->exp;
			node->next = make_term(0, 0);
			node = node->next;
			a = a->next;
		}
		else if (b){
			node->coeff = b->coeff;
			node->exp = b->exp;
			node->next = make_term(0, 0);
			node = node->next;
			b = b->next;
		}
	}	
	return head; 
}

//Is a bool function that checks if two polynomials are the same, returning true if they are and false if they are not
bool is_equal(polynomial *a, polynomial *b){
	
	//As long as a and b are not NULL then loop through checking both to see if their exponents are the same, and if they are not return false
	while (a && b){
		if (a->exp != b->exp || a->coeff != b->coeff){
			return false;
		}
		a = a->next;
		b = b->next;
	}

	//Checking to see if somehow the while loop did not catch everyhting, thus if there is still an a or b after exiting return false
	if (a  || b ){
		return false;
	}

	return true;
}

//Function that will apply the transform function to a struct term*
void apply_to_each(polynomial *p, void (*transform)(struct term *)){

	//If there is p then loop through calling transform on p until p is eventuall NULL
	while(p){
		transform(p);
		p = p->next;
	}
}

//Function that will evaluate a polynomial witht a given double
double eval_poly(polynomial *p, double x){

	double result = 0;

	//Loops through while p is not NULL setting result equal to p's coefficient times the pow(x, p->exp) eventuall returning result once p is NULL
	while(p){
		result += p->coeff*pow(x, p->exp);
		p = p->next;
	}
	return result;
}






